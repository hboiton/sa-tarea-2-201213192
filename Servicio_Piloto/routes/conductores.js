var express = require('express');
var router = express.Router();

/* GET home page.
    esta pagian nos devuelbe una lista de conductores disponibles
*/
router.get('/', function(req, res, next) {
  var datos=
  {
    conductores:[
      {nombre :" Conductor Uno",   carro: "toyota",linea :"yaris", placa: "P-00012", ubicacion : "zona 8"},
      {nombre :" Conductor Dos",   carro: "toyota",linea :"corola", placa: "P-00015", ubicacion : "zona 2"},
      {nombre :" Conductor Tres",  carro: "kia",linea :"rio", placa: "P-00019", ubicacion : "zona 10"},
      {nombre :" Conductor Cuatro",carro: "kia",linea :"picanto", placa: "P-00017", ubicacion : "zona 4"},
      {nombre :" Conductor Cinco", carro: "toyota",linea :"hallux", placa: "P-00010", ubicacion : "zona 9"},
      {nombre :" Conductor Seis",  carro: "mazda",linea :"dos", placa: "P-00019", ubicacion : "zona 3"},
      {nombre :" Conductor Siete", carro: "toyota",linea :"yaris", placa: "P-00018", ubicacion : "zona 6"},
      
    ]
  }
  res.send(datos);
});

module.exports = router;
