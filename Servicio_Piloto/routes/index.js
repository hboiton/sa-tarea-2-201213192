var express = require('express');
var router = express.Router();

/* GET home page. 
  en esta pagian mostramos los conductores disponibles 
*/
router.get('/', function(req, res, next) {
  var datos=
  {
    titulo:"Listado de conductores activos",
    conductores:[
      {id:1,nombre :" Conductor Uno",   carro: "toyota",linea :"yaris", placa: "P-00012", ubicacion : "zona 8"},
      {id:2,nombre :" Conductor Dos",   carro: "toyota",linea :"corola", placa: "P-00015", ubicacion : "zona 2"},
      {id:3,nombre :" Conductor Tres",  carro: "kia",linea :"rio", placa: "P-00019", ubicacion : "zona 10"},
      {id:4,nombre :" Conductor Cuatro",carro: "kia",linea :"picanto", placa: "P-00017", ubicacion : "zona 4"},
      {id:5,nombre :" Conductor Cinco", carro: "toyota",linea :"hallux", placa: "P-00010", ubicacion : "zona 9"},
      {id:6,nombre :" Conductor Seis",  carro: "mazda",linea :"dos", placa: "P-00019", ubicacion : "zona 3"},
      {id:7,nombre :" Conductor Siete", carro: "toyota",linea :"yaris", placa: "P-00018", ubicacion : "zona 6"},
      
    ]
  }
  res.render('index', datos);
});

module.exports = router;
