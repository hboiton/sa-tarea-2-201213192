const  express = require('express');
const app = express();
const bodyParser = require('body-parser')
var async  = require('express-async-await')
var fetch = require('node-fetch')

app.use(express.static(__dirname+'/public'));
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/',function(req,res){

     res.send('Pagina principal ESB')
});

/*
    en esta solicitud se procesa toda la informanas que nos manda el ESB
    el listado de conductores disponibes y se calcula la ruta y tiempo de viaje
*/
app.get('/conductor',function(req,res){
     var datos= req.query.datos;// lista de conductores
     datos=JSON.parse(datos)// convertir a json
     var indice=obtenervaloresRandom(0,6);
     var conuductor=datos.conductores[indice]
     // respuesta del modulo
     var respuesta={
         conductor:conuductor,
         tiempo: obtenervaloresRandom(10,20)+ " minutos",
         distancia: obtenervaloresRandom (2, 10)+ " Kilometros"
     }
     res.send(respuesta)
});
//devuelve una valor random entre un rango de numeros
function obtenervaloresRandom(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }


app.listen(8080,function(){
    console.log(" El Servicio de ruta esta activo en puerto 8080")
});