var express = require('express');
var router = express.Router();
var async  = require('express-async-await')
var fetch = require('node-fetch')


/* GET home page. */
router.post('/', async function(req, res, next) {
    //esta funcion hacemos la solicitud al ESB para obtener la estimacion de la ruta y asignar conductor
    function getDatosViaje()
   {
       return fetch('http://localhost:3001/getviaje');
   }
   const pocesandoViaje=async()=>
   {
       const datosViaje = await getDatosViaje() //Solicitamos datos para un nuevo viaje y nos responde
       const jsonViaje = await datosViaje.json() //comvertimos los datos resividos a un json
       // estos son los datos que vamos a mostrar en la pagina del cliente
       /*
            la variable datos en donde almacenamos la informacion que vamos a mostras al 
            cliente sobre su viaje
       */
       var datos =
        {
            nombre:req.body.nombre,
            ubicacion:req.body.ubicacion,
            destino:req.body.destino,
            datos:jsonViaje
        }
        res.render('viaje',  datos );
   }
   pocesandoViaje();  
});

module.exports = router;