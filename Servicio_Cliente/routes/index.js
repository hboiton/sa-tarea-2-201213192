var express = require('express');
var router = express.Router();

/* GET home page.
   Paginas para solicitar datos de nuevo Viaje
*/
router.get('/', function(req, res, next) {

  res.render('index', { title: 'Solucitud  viaje' });
});

module.exports = router;
