const  express = require('express');
const app = express();
const bodyParser = require('body-parser')
var async  = require('express-async-await')
var fetch = require('node-fetch')

app.use(express.static(__dirname+'/public'));
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/',function(req,res){

     res.send('Pagina principal ESB')
});

app.get('/getviaje',async function(req,res)
{

    function getconductores() //en esta funcion pedimos al servicio de conductores que nos de el listado de conductores disponibles
    {
        return fetch('http://localhost:8050/conductores')
    }
    function getRuta(datos) // es esta fucion mandamos el listado de conductores disponives con ubicacion y nos debuelce la ruta y conductor asignado
    {
        return fetch('http://localhost:8080/conductor?datos='+datos)
    }
    const procesandoConductores= async () =>
    {
        const datoscondutores = await getconductores() // solicitar conductores
        const datosjsonConductores=await datoscondutores.json() // pasar a un json
        const datosRuta = await getRuta(JSON.stringify(datosjsonConductores)) // solicitamos una ruta y conductor mejor ubicado
        const datosJsonRuta = await datosRuta.json()// pasamos a json la solicitud
        res.send(datosJsonRuta)
    }
    procesandoConductores()
})

// funcion donde se indica que en que puerto va estar ESB
app.listen(3001,function(){
    console.log(" El servicio ESB esta arriba en el puerto 3001")
});
